import fileinput
import json

object = json.loads(fileinput.input("file.txt").readline())
counters = [[0,0],[0,0],[0,0],[0,0]]

for course in object:
	for date in course['dates']:
		cycle_n = date['cycle_n'] - 1
		accepts_underage = course['accepts_underage']
		counters[cycle_n][0] += 1
		if accepts_underage:
			counters[cycle_n][1] += 1

i = 1
for cycle in counters:
	print("{0} цикл: {1} мастерских {2} из них школьных".format(i, cycle[0], cycle[1]))
	i += 1	
